package pl.connectis.zajecia_11_10;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ExampleTest {

	@BeforeAll
	public static void setUpAll() {
		System.out.println("@BeforeAll: Setting all up!");
	}

	@BeforeEach
	public void setUp() {
		System.out.println("@BeforeEach: Setting up!");
	}

	@AfterEach
	public void tearDown() {
		System.out.println("@AfterEach: Tearing down!");
	}

	@AfterAll
	public static void tearAllDown() {
		System.out.println("@AfterAll: Tearing all down!");
	}

	@Test
	public void exampleTest() {
		System.out.println("Example test run.");
		boolean isSuccess = true;
		Assertions.assertTrue(isSuccess);

		boolean isError = false;
		Assertions.assertFalse(isError);

		String hello = null;
		Assertions.assertNull(hello);

		hello = "Hello world!";
		Assertions.assertNotNull(hello);
		Assertions.assertEquals("Hello world!", hello);
		Assertions.assertNotEquals("Witaj swiecie!", hello);
	}

	@Test
	public void listTest() {
		System.out.println("List test run.");
		List<String> list = new ArrayList<>();
		Assertions.assertTrue(list.isEmpty());

		String abcString = "abc";
		list.add(abcString);
		list.add("def");

		Assertions.assertEquals(2, list.size());
		Assertions.assertEquals(abcString, list.get(0));
	}
}
