package pl.connectis.zajecia_11_10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LoginServiceTest {

	private static final String SUCCESS = "Sukces";
	private static final String USER_NOT_FOUND = "Nie znaleziono uzytkownika!";
	private static final String USER_EXISTS = "Taki uzytkownik juz istnieje!";
	private static final String WRONG_PASSWORD = "Zle haslo!";
	private static final String EMPTY_LOGIN_OR_PASSWORD = "Nie podano hasla lub loginu!";
	private static final String PASSWORD_IS_NOT_ALLOWED = "Podane haslo nie jest dozwolone!";

	private LoginService loginService;

	@BeforeEach
	public void setUp() {
		System.out.println("Running test.");
		loginService = new LoginService();
	}

	@Test
	public void givenCorrectLoginAndPassword_shouldReturnSuccess() {
		String message = loginService.login("login1", "Test1234");
		Assertions.assertEquals(SUCCESS, message);
	}

	@Test
	public void givenCorrectLoginAndWrongPassword_shouldReturnWrongPassword() {
		String message = loginService.login("login1", "Test1234567");
		Assertions.assertEquals(WRONG_PASSWORD, message);
	}

	@Test
	public void givenCorrectLoginAndEmptyPassword_shouldReturnEmptyPassword() {
		String message = loginService.login("login1", "");
		Assertions.assertEquals(EMPTY_LOGIN_OR_PASSWORD, message);
	}

	@Test
	public void givenNullLoginAndCorrectPassword_shouldReturnEmptyLogin() {
		String message = loginService.login(null, "Test1234");
		Assertions.assertEquals(EMPTY_LOGIN_OR_PASSWORD, message);
	}

	@Test
	public void givenWrongLoginAndCorrectPassword_shouldReturnUserNotFound() {
		String message = loginService.login("login123", "Test1234");
		Assertions.assertEquals(USER_NOT_FOUND, message);
	}

	@Test
	public void givenCorrectLoginAndCorrectPassword_shouldRegisterAndReturnSuccess() {
		String message = loginService.register("login123", "Test1234");
		Assertions.assertEquals(SUCCESS, message);
	}

	@Test
	public void givenCorrectLoginAndEmptyPassword_shouldNotRegisterAndReturnEmptyPassword() {
		String message = loginService.register("login123", "");
		Assertions.assertEquals(EMPTY_LOGIN_OR_PASSWORD, message);
	}

	@Test
	public void givenCorrectLoginAndWrongPassword_shouldNotRegisterAndReturnWrongPassword() {
		String message = loginService.register("login123", "testabc");
		Assertions.assertEquals(PASSWORD_IS_NOT_ALLOWED, message);
	}

	@Test
	public void givenExistingLoginAndCorrectPassword_shouldNotRegisterAndReturnUserExists() {
		String message = loginService.register("login1", "Test1234");
		Assertions.assertEquals(USER_EXISTS, message);
	}
}
