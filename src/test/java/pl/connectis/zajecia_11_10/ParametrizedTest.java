package pl.connectis.zajecia_11_10;

import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

public class ParametrizedTest {

	private LoginService loginService;

	@BeforeEach
	private void setUp() {
		loginService = new LoginService();
	}

	@ParameterizedTest
	@ValueSource(strings = {"Hello", "World", "Witaj", "Swiecie"})
	void testParameterNotNull(String param) {
		Assertions.assertNotNull(param);
	}

	@ParameterizedTest
	@MethodSource("loginDataProvider")
	void givenCorrectLoginAndPassword_shouldReturnSuccess(String login,
			String password) {
		String message = loginService.login(login, password);
		Assertions.assertEquals("Sukces", message);
	}

	private static Stream loginDataProvider() {
		return Stream.of(
				Arguments.of("login1", "Test1234"),
				Arguments.of("login2", "test1234"),
				Arguments.of("adam", "adam1234"),
				Arguments.of("ewa", "ewa12345")
		);
	}
}
