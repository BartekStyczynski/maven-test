package pl.connectis.zajecia_11_09;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MavenTestTest {

	@Test
	public void testHello() {
		//given
		System.out.println("Running test...");
		MavenTest mavenTest = new MavenTest();

		//when
		String hello = mavenTest.getHello();

		//then
		Assertions.assertEquals("hello", hello);
	}
}
