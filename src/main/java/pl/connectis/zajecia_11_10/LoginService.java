package pl.connectis.zajecia_11_10;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public class LoginService {

	private final Map<String, String> loginPassMap = new HashMap<>();

	private static final String SUCCESS = "Sukces";
	private static final String USER_NOT_FOUND = "Nie znaleziono uzytkownika!";
	private static final String USER_EXISTS = "Taki uzytkownik juz istnieje!";
	private static final String WRONG_PASSWORD = "Zle haslo!";
	private static final String EMPTY_LOGIN_OR_PASSWORD = "Nie podano hasla lub loginu!";
	private static final String PASSWORD_IS_NOT_ALLOWED = "Podane haslo nie jest dozwolone!";

	public LoginService() {
		loginPassMap.put("login1", "Test1234");
		loginPassMap.put("login2", "test1234");
		loginPassMap.put("adam", "adam1234");
		loginPassMap.put("ewa", "ewa12345");
	}

	public String login(String login, String pass) {
		if (StringUtils.isAnyBlank(login, pass)) {
			return EMPTY_LOGIN_OR_PASSWORD;
		}

		if (!loginPassMap.containsKey(login)) {
			return USER_NOT_FOUND;
		}

		if (!pass.equals(loginPassMap.get(login))) {
			return WRONG_PASSWORD;
		}

		return SUCCESS;
	}

	public String register(String login, String pass) {
		if (StringUtils.isAnyBlank(login, pass)) {
			return EMPTY_LOGIN_OR_PASSWORD;
		}

		if (loginPassMap.containsKey(login)) {
			return USER_EXISTS;
		}

		if (!pass.matches("(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{8,})")) {
			return PASSWORD_IS_NOT_ALLOWED;
		}
		loginPassMap.put(login, pass);
		return SUCCESS;
	}
}
